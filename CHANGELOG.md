<a name="unreleased"></a>
## [Unreleased]


<a name="0.1.1"></a>
## [0.1.1] - 0001-01-01

<a name="0.1.0"></a>
## [0.1.0] - 2019-10-04
### Install
- Install Epel only for CentOs

### Update
- Update Readme


<a name="0.0.3"></a>
## [0.0.3] - 2019-08-09
### Add
- add a step for epel installation on redhat family
- add Fedora var file for package name


<a name="0.0.2"></a>
## [0.0.2] - 2019-08-09
### Add
- add fedora as availlable in meta information
- add epel repo on redhat based distrib

### Add
- Add LICENSE

### Merge
- Merge branch 'master' of gitlab.com:FanchG/ansible-putty


<a name="0.0.1"></a>
## 0.0.1 - 2019-08-09
### First
- first commit

### Update
- update readme with true content


[Unreleased]: https://gitlab.com/ifb-elixirfr/ansible-roles/ansible-putty/compare/0.1.1...HEAD
[0.1.1]: https://gitlab.com/ifb-elixirfr/ansible-roles/ansible-putty/compare/0.1.0...0.1.1
[0.1.0]: https://gitlab.com/ifb-elixirfr/ansible-roles/ansible-putty/compare/0.0.3...0.1.0
[0.0.3]: https://gitlab.com/ifb-elixirfr/ansible-roles/ansible-putty/compare/0.0.2...0.0.3
[0.0.2]: https://gitlab.com/ifb-elixirfr/ansible-roles/ansible-putty/compare/0.0.1...0.0.2
