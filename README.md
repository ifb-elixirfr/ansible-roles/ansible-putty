Putty
=====

A role to install Putty on Linux distrib


Dependencies
------------

[Requirements](molecule/default/requirements.yml)


Variables
---------

[Role Variables](defaults/main.yml)

[Dependencie Variables](vars/main.yml)


Example Playbook
----------------

[Test Playbook](molecule/default/playbook.yml)


License
-------

[License](LICENSE)
